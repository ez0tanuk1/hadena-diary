<?php
define('ARTICLE_NUM', 250);
define('FLAG', getenv('FLAG') ?: 'FLAG{dummy}');
assert(preg_match('/^FLAG\{[0-9a-z_-]+\}$/', FLAG), 'flag should be FLAG{[0-9A-Za-z_-]+}');

$pdo = new PDO('sqlite:db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$pdo->exec("
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS star;
");

$pdo->exec("
CREATE TABLE IF NOT EXISTS user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT,
    password TEXT
);

CREATE TABLE IF NOT EXISTS article (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    body TEXT,
    user_id INTEGER,
    published INTEGER
);

CREATE TABLE IF NOT EXISTS star (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER,
    article_id INTEGER,
    text TEXT
);
");

$stmt = $pdo->prepare('INSERT INTO user(username, password) VALUES ("admin", ?);');
$stmt->execute([bin2hex(random_bytes(64))]);

$stmt = $pdo->prepare('INSERT INTO article(title, body, published, user_id) VALUES (?, ?, ?, ?);');
$flag_id = random_int(1, ARTICLE_NUM - 10);
for ($i = 0; $i < ARTICLE_NUM; $i++) {
    if ($i === $flag_id) {
        $article = ['FLAG', '<p>' . FLAG . '</p>', 0, 1];
    } else {
        $article = ['Lorem ipsum', '<p>' . bin2hex(random_bytes(64)) . '</p>', 1, 1];
    }

    $stmt->execute($article);
}