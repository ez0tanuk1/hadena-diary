<?php
$pdo = new PDO('sqlite:../db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

session_start();

if (isset($_POST['id']) && isset($_POST['where'])) {
    if (!isset($_SESSION['username'])) {
        $_SESSION['flash'] = 'Please log in or register';
        exit;
    }

    $id = $_POST['id'];
    $where = $_POST['where'];

    $stmt = $pdo->prepare("SELECT body FROM article WHERE id = ?");
    $stmt->execute([$id]);
    $result = $stmt->fetchAll();

    if (count($result) === 0) {
        $_SESSION['flash'] = 'Article not found';
        exit;
    }

    if (strpos($result[0]['body'], $where) === false) {
        $_SESSION['flash'] = 'Substring not found';
        exit;
    }

    $stmt = $pdo->prepare('INSERT INTO star(user_id, article_id, text) VALUES (?, ?, ?);');
    $stmt->execute([$_SESSION['userid'], $id, $where]);
    exit;
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $stmt = $pdo->prepare("SELECT user_id, published FROM article WHERE id = ?");
    $stmt->execute([$id]);
    $result = $stmt->fetchAll();

    if (count($result) === 0) {
        $_SESSION['flash'] = 'Article not found';
        exit;
    }

    if ($result[0]['published'] === 0 && (!isset($_SESSION['username']) || $result[0]['user_id'] !== $_SESSION['userid'])) {
        $_SESSION['flash'] = 'Unauthorized';
        exit;
    }

    $stmt = $pdo->prepare("SELECT username, text FROM star INNER JOIN user ON star.user_id = user.id WHERE article_id = ?");
    $stmt->execute([$id]);
    $result = $stmt->fetchAll();
    
    header('Content-Type: application/json');
    echo json_encode($result);
}
?>