(async () => {
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

    const articleBody = document.getElementById('body');
    const starmenu = document.getElementById('starmenu');
    const stargazers = document.getElementById('stargazers');
    let selected = null;

    const id = new URLSearchParams(location.search).get('id');

    const originalBody = articleBody.innerHTML;
    fetch('/star.php?id=' + id).then(r => r.json()).then(res => {
        for (const { username, text } of res) {
            const span1 = document.createElement('span');
            span1.title = username;
            span1.classList.add('star');

            const span2 = document.createElement('span');
            span2.textContent = username.slice(0, 2);
            span1.appendChild(span2);

            const star = document.createElement('img');
            star.src = '/static/star.png';
            star.classList.add('star-img');
            span1.appendChild(star);

            span1.addEventListener('mouseover', () => {
                articleBody.innerHTML = originalBody.replace(text, `<span class="highlight">${text}</span>`);
            }, false);

            span1.addEventListener('mouseout', () => {
                articleBody.innerHTML = originalBody;
            }, false);

            stargazers.appendChild(span1);
        }
    });

    document.addEventListener('selectionchange', e => {
        const selection = document.getSelection();
        const { focusNode } = selection;
        if (focusNode != null && focusNode.parentNode === starmenu) {
            return;
        }

        if (focusNode == null || focusNode.parentNode == null || focusNode.parentNode.parentNode !== articleBody) {
            selected = null;
            return;
        }
    
        let { baseOffset: start, focusOffset: end } = selection;
        if (start > end) {
            const tmp = start;
            start = end;
            end = tmp;
        }
    
        selected = focusNode.textContent.slice(start, end);
    }, false);
    
    document.body.addEventListener('click', e => {
        if (!e.target.closest('#body') && !e.target.closest('#starmenu') && selected == null) {
            starmenu.classList.add('hidden');
        }
    }, false);

    articleBody.addEventListener('mouseup', e => {
        starmenu.style.position = 'absolute';
        starmenu.style.top = `${e.pageY - 16}px`;
        starmenu.style.left = `${e.pageX + 10}px`;
        starmenu.classList.remove('hidden');
    }, false);

    starmenu.addEventListener('click', async () => {
        starmenu.textContent = 'Pending...';

        const formData = new FormData;
        formData.append('id', id);
        formData.append('where', selected);
        await fetch('/star.php', {
            method: 'POST',
            body: formData
        });

        await sleep(250);
        starmenu.textContent = 'Done!';
        await sleep(250);
        location.reload();
    }, false);
})();