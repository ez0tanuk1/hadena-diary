<?php
session_start();

$pdo = new PDO('sqlite:../db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$stmt = $pdo->prepare("SELECT article.id AS id, title, published, username FROM article INNER JOIN user ON user.id = article.user_id ORDER BY article.id desc LIMIT 5");
$stmt->execute();
$articles = $stmt->fetchAll();
?>
<?php include('includes/header.php'); ?>
    <article class="container">
        <?php if (isset($_SESSION['flash'])) { ?>
        <div class="error"><?= $_SESSION['flash'] ?></div>
        <?php unset($_SESSION['flash']); } ?>
        <h2>Welcome to Hadena Diary!</h2>
        <p>You can post articles and give Hadena Stars<img src="/static/star.png"> to your friends here!</p>
        <p>Please <a href="/login.php">log in or register</a> to use Hadena Diary.</p>
        <h2>How to use Hadena Stars</h2>
        <p>1. Select text you want to add a Hadena Star, and click "Add star".</p>
        <img src="/static/howto1.png">
        <p>2. Hadena Star will be shown on the article.</p>
        <img src="/static/howto2.png">
        <p>3. Selected text will be highlighted when the cursor is on your star.</p>
        <img src="/static/howto3.png">
        <h2>Latest articles</h2>
        <ul>
<?php foreach($articles as $article) { ?>
            <li><a href="/article.php?id=<?= $article['id'] ?>"><?= $article['title'] ?></a><?= $article['published'] === 0 ? '🔒' : '🔓' ?> by <?= $article['username'] ?></li>
<?php } ?>
        </ul>
    </article>
<?php include('includes/footer.php'); ?>