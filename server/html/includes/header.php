<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hadena Diary</title>
    <link rel="stylesheet" href="/static/style.css">
</head>
<body>
    <header class="container header">
        <h1><a href="/index.php"><span class="rainbow">Hadena Diary</span></a></h1>
        <nav class="navbar">
            <?php if (isset($_SESSION['username'])) { ?>
            Logged in as <?= $_SESSION['username'] ?>.<br>
            <?php } ?>
            <a href="/index.php">Home</a> | 
            <?php if (isset($_SESSION['username'])) { ?>
            <a href="/edit.php">New Article</a> |
            <a href="/logout.php">Log out</a>
            <?php } else { ?>
            <a href="/login.php">Log in</a>
            <?php } ?>
        </nav>
    </header>