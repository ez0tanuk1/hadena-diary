<?php
$pdo = new PDO('sqlite:../db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

session_start();

if (!isset($_GET['id'])) {
    $_SESSION['flash'] = 'Article not found';
    header('Location: /index.php');
    exit;
}
$id = intval($_GET['id'], 10);

$stmt = $pdo->prepare("SELECT title, body, username, published FROM article INNER JOIN user ON user.id = user_id WHERE article.id = ?");
$stmt->execute([$id]);
$result = $stmt->fetchAll();

if (count($result) === 0) {
    $_SESSION['flash'] = 'Article not found';
    header('Location: /index.php');
    exit;
}
$article = $result[0];

if ($article['published'] === 0 && (!isset($_SESSION['username']) || $article['username'] !== $_SESSION['username'])) {
    $_SESSION['flash'] = 'Unauthorized';
    header('Location: /index.php');
    exit;
}
?>
<?php include('includes/header.php'); ?>
    <article class="container">
        <?php if (isset($_SESSION['flash'])) { ?>
        <div class="error"><?= $_SESSION['flash'] ?></div>
        <?php unset($_SESSION['flash']); } ?>
        <h2><?= $article['title'] ?></h2>
        <small>posted by <?= $article['username'] ?></small>
        <div id="body"><?= $article['body'] ?></div>
        <div id="stargazers"></div>
    </article>
    <div id="starmenu" class="hidden">
        Add star
    </div>
    <script src="/static/star.js"></script>
<?php include('includes/footer.php'); ?>