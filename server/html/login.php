<?php
$pdo = new PDO('sqlite:../db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

session_start();

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $stmt = $pdo->prepare("SELECT * FROM user WHERE username = ?");
    $stmt->execute([$username]);
    $user = $stmt->fetchAll();

    if (count($user) === 1) {
        if (hash_equals($user['password'], $password)) {
            $_SESSION['username'] = $username;
            $_SESSION['userid'] = $user['id'];
        } else {
            $_SESSION['flash'] = 'Wrong password';
            header('Location: /login.php');
            exit;
        }
    } else {
        if (!preg_match('/^[0-9A-Za-z_-]+$/', $username)) {
            $_SESSION['flash'] = 'Hacking attempt detected';
            header('Location: /login.php');
            exit;
        }

        $stmt = $pdo->prepare('INSERT INTO user(username, password) VALUES (?, ?);');
        $stmt->execute([$username, $password]);

        $_SESSION['username'] = $username;
        $_SESSION['userid'] = (int) $pdo->lastInsertId();
    }

    header('Location: /index.php');
}
?>
<?php include('includes/header.php'); ?>
    <div class="container">
        <?php if (isset($_SESSION['flash'])) { ?>
        <div class="error"><?= $_SESSION['flash'] ?></div>
        <?php unset($_SESSION['flash']); } ?>
        <form action="/login.php" method="post">
            <div>
                <label for="username">Username:</label>
                <input type="text" name="username">
            </div>
            <div>
                <label for="password">Password:</label>
                <input type="password" name="password">
            </div>
            <div>
                <input type="submit" value="Log in / Register">
            </div>
        </form>
    </div>
<?php include('includes/footer.php'); ?>