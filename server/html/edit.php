<?php
$pdo = new PDO('sqlite:../db.db');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

session_start();

if (!isset($_SESSION['username'])) {
    $_SESSION['flash'] = 'Please log in or register';
    header('Location: /login.php');
    exit;
}

if (isset($_POST['title']) && isset($_POST['body'])) {
    $title = $_POST['title'];
    $body = $_POST['body'];
    $publish = isset($_POST['publish']);

    $title = htmlspecialchars($title, ENT_QUOTES);
    $body = htmlspecialchars($body, ENT_QUOTES);
    $body = '<p>' . str_replace("\n", '</p><p>', $body) . '</p>';

    $stmt = $pdo->prepare('INSERT INTO article(title, body, published, user_id) VALUES (?, ?, ?, ?);');
    $stmt->execute([$title, $body, +$publish, $_SESSION['userid']]);

    header('Location: /article.php?id=' . $pdo->lastInsertId());
    exit;
}
?>
<?php include('includes/header.php'); ?>
    <div class="container">
        <?php if (isset($_SESSION['flash'])) { ?>
        <div class="error"><?= $_SESSION['flash'] ?></div>
        <?php unset($_SESSION['flash']); } ?>
        <form action="/edit.php" method="post">
            <div>
                <label for="title">Title:</label>
                <input type="text" name="title">
            </div>
            <div>
                <label for="body">Content:</label>
                <textarea name="body" cols="30" rows="10"></textarea>
            </div>
            <div>
                <label for="publish">Make this article visible to everyone?</label>
                <input type="checkbox" name="publish">
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>
<?php include('includes/footer.php'); ?>